# Bash script for converting FLV videos to MP4 
# Can be made to work with any ext http://stackoverflow.com/questions/965053/extract-filename-and-extension-in-bash
for FILE in "$@"
do
  FILE_NO_EXT=$(basename "$FILE" .flv)
  OUT_DIR=$(dirname "$FILE")
  FILE_OUT="${OUT_DIR}/${FILE_NO_EXT}.mp4"
  FILE_OUT_TEMP="${OUT_DIR}/${FILE_NO_EXT}-converting.mp4"

  echo "FILE: ${FILE}"
  echo "FILE_NO_EXT: ${FILE_NO_EXT}"
  echo "OUT_DIR: ${OUT_DIR}"
  echo "FILE_OUT: ${FILE_OUT}"
  echo "FILE_OUT_TEMP: ${FILE_OUT_TEMP}"

  /usr/local/bin/growlnotify --message "Converting: $FILE"
  /usr/local/bin/ffmpeg -threads 2 -i "$FILE" "${FILE_OUT_TEMP}"
  rc=$?
  if [[ $rc != 0 ]]; then
    /usr/local/bin/growlnotify --message "Conversion failed: $FILE"
    rm "${FILE_OUT_TEMP}"
    exit $rc
  fi
  mv "$FILE_OUT_TEMP" "$FILE_OUT"
  /usr/local/bin/growlnotify --message "Converted: $FILE"
done

